// System
// - This script is used for various system-related tasks (e.g., updating).


// Import
import_code("/opt/lib/std/libraries.src")
import_code("/opt/lib/std/file.src")


// Current User
// - Who the script is being run as.
current_user = active_user

// Hardcoded Paths
PATH_PASSWD = "/etc/passwd"

// CLI
CLI_LIB_VERSIONS = "--lib-versions"
CLI_UPDATE = "--update"
CLI_SECURE = "--secure"


// Usage
if params.len == 0 or params[0] == "--help" then
    print("system")
    print(CLI_LIB_VERSIONS + " => print library versions")
    print(CLI_UPDATE + " => update local system (same as `apt-get`)")
    print(CLI_SECURE + " => remove mail / bank / passwd files")
    exit
end if

// Determine Action
action = params[0]

// Check if we are root
if current_user != "root" then
    exit("This script requires root privileges; exiting.")
end if


// Action: Library - Versions
if action == CLI_LIB_VERSIONS then
    l = new Libraries

    // Ensure metaxploit
    if not l.has_metaxploit then
        exit("Metaxploit library is required; unable to continue")
    end if

    // Load libraries
    if not l.load_libraries then
        exit("Failed to load libraries; check path and permissions")
    end if

    // Print library versions
    for lib in l.get_library_versions
        print(lib)
    end for


// Action: Library - Update
else if action == CLI_UPDATE then
    print("Not Implemented")


// Action: Secure
else if action == CLI_SECURE then
    f = new _File

    // /etc/passwd
    print("-- Processing static files --")
    print("Checking: " + PATH_PASSWD)
    if not f.open(PATH_PASSWD, "w") then
        print("File does not exist; nothing to do")
    else if not f.delete then
        print("File exists but failed to remove; please check manually")
    end if
    f.close
    print

    // Get a list of all user directories
    d = new _File
    users = []
    if not d.open("/home", "r") then
        exit("/home not found or inaccessible; exiting")
    else
        users = d.get_folders
    end if

    // Iterate through user directories
    for user in users
        print("-- Processing the following user account: " + user.get_name + " --")

        // Paths
        path_bank = "/home/" + user.get_name + "/Config/Bank.txt"
        path_mail = "/home/" + user.get_name + "/Config/Mail.txt"

        // Bank.txt
        print("Checking: " + path_bank)
        if not f.open(path_bank, "w") then
            print("File does not exist; nothing to do")
        else if not f.delete then
            print("File exists but failed to remove; please check manually")
        else
            print("File removed")
        end if
        print

        // Mail.txt
        print("Checking: " + path_mail)
        if not f.open(path_mail, "w") then
            print("File does not exist; nothing to do")
        else if not f.delete then
            print("File exists but failed to remove; please check manually")
        else
            print("File removed")
        end if
        print
    end for

    // Cleanup
    d.close
    f.close

// Unknown
else
    print("system")
    print(CLI_LIB_VERSIONS + " => print library versions")
    print(CLI_UPDATE + " => update local system (same as `apt-get`)")
    print(CLI_SECURE + " => remove mail / bank / passwd files")
    exit
end if
